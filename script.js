// Create a function that accepts a word as a parameter, and returns the same phrase
// in reverse only if the number of vowels in the word is divisible by 2.
function vowelsCheck (word) {
    let words = word.toLowerCase()
    let finalWord = words.split("")
    
    let numberOfVowels = 0 
    let vowels = ["a","e","i","o","u"]

    for(let i = 0; i < finalWord.length; i++){
        if( vowels.indexOf(finalWord[i]) != -1 ){
            numberOfVowels = numberOfVowels + 1
        } else {
            numberOfVowels 
        }
    }

    if(numberOfVowels % 2 == 0){
        return(word.split("").reverse().join("")) 
    } else {
        return(word)
    }
}

console.log(vowelsCheck("Love"))
console.log(vowelsCheck("Algebra"))


// A prime number is a number that has only 2 factors. Create a function that
// determines if a number is a prime number.

function isPrime (number) {
    if(number == 2){
        return("prime number")
    } else if ( number % 2 == 0) {
        return("not a prime number")
    } else {
        return("prime number")
    }
}

console.log(isPrime(2))
console.log(isPrime(100))


//Create a function that accepts a number from 1-100 and returns the number in words.

//function number to words 


function numToWords(number){
    
    let numGreaterThan10ButLessThan20 = 0   
    
    if(number < 10){
        return belowTen(number)
    } 
    
    if( 10 < number < 20){
        numGreaterThan10ButLessThan20 += number 
    }

    if(number === 100){
        return("One Hundered")
    }


    switch(numGreaterThan10ButLessThan20){

        case 11:
        return "Eleven";
        break;

        case 12: 
        return "Twelve";
        break;

        case 13:
        return "Thirteen";
        break;

        case 14: 
        return "Fourteen";
        break;

        case 15:
        return "Fifteen";
        break;

        case 16: 
        return "Sixteen";
        break ;  

        case 17: 
        return "Seventeen";
        break;

        case 18: 
        return "Eighteen"  ;
        break;

        case 19:
        return "Nineteen";
        break;

    }

    let remainder = number % 10 
    let digit = Math.trunc(number / 10) // removes decimal

    if (remainder === 0 ){
        return (numberInTenths(digit))
    } else {
        let tenthsValue  = numberInTenths(digit)
        let onesValue = belowTen(remainder)

        return (`${tenthsValue} ${onesValue}`)
    }
}

function belowTen (numLessThan10){
    switch(numLessThan10){
        case 1: 
        return "One" ;
        break;
        
        case 2:
        return "Two";
        break;

        case 3:
        return "Three" ;
        break;

        case 4: 
        return "Four";
        break;

        case 5: 
        return "Five";
        break;

        case 6:
        return "Six";
        break;

        case 7: 
        return "Seven";
        break;

        case 8: 
        return "Eight";
        break;

        case 9:
        return "Nine";
        break;
    }
}
    
function numberInTenths(numInTenths){
    switch(numInTenths){
    
        case 1:
        return "Ten"
        break;

        case 2:
        return "Twenty"
        break;
        
        case 3:
        return "Thirty"
        break;
        
        case 4: 
        return "Forty"
        break;
        
        case 5: 
        return "Fifty"
        break;
        
        case 6:
        return "Sixty"
        break;
        
        case 7:
        return "Seventy"
        break;
        
        case 8:
        return "Eighty"
        break;
        
        case 9:
        return "Ninty"
        break;
        
        case 10:
        return "One hundred"
        break;
    }
}

console.log(numToWords(3))
console.log(numToWords(18))
console.log(numToWords(100))
console.log(numToWords(20))
console.log(numToWords(33))
    





